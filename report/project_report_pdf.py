from odoo.http import request
from odoo import models, api, _
from odoo.modules.module import get_module_path
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

class ProjectReportParser(models.AbstractModel):
    _name = 'report.bigbang.project_report_template'


    def get_selection_label(self, object, field_name, field_value):
        return _(dict(self.env[object].fields_get(allfields=[field_name])[field_name]['selection'])[field_value])

    
    def draw_pie_chart(self, tasks, project_id):
        data = dict()
        for x in tasks:
            if x['stage']:
                data[ x['stage'] ] = data.get(x['stage'], 0) + 1  
        
        if len(data) == 0:
            return False
         
        labels, quantity = list(data.keys()), list(data.values())

        colors = ['gold', 'lightskyblue', 'lightcoral', 'purple', 'yellowgreen']
        plt.pie(quantity, colors=colors, autopct='%1.1f%%',
                shadow=False, startangle=90, labeldistance=1.1)
        plt.title('Entregables por etapa')   
        plt.axis('equal')
        plt.legend(labels=labels)
        path = get_module_path('bigbang')
        file_img = '/static/src/img/pie_'+str(project_id)+'.png'
        plt.savefig(path + file_img, bbox_inches='tight')
        plt.close() 
        return '/bigbang/'+file_img

    def draw_bar_chart(self, tasks, project_id):
        data_count = dict()
        for x in tasks:
            if x['milestone']:
                data_count[ x['milestone'] ] = data_count.get(x['milestone'], 0) + 1  

        if len(data_count) == 0:
            return False

        data = dict()
        for x in tasks:
            data[ x['milestone'] ] = data.get(x['milestone'], 0) + x['progress']   

        for k in data:
            data[k] = data[k] / data_count[k]

        data_orders = sorted(data.items(), key=lambda x: x[0], reverse = True)
        labels, quantity = list(zip(*data_orders))
        
        plt.xlim(0, 105)
        plt.barh(labels, quantity)     
        plt.title('Progreso de Hitos')   
        plt.ylabel('Hitos')
        plt.xlabel('Progreso')
        for i, v in enumerate(quantity):
            plt.text(v + 3, i + .25, str(round(v))+'%',
                color = 'blue', fontweight = 'bold')

        path = get_module_path('bigbang')
        file_img = '/static/src/img/bar_'+str(project_id)+'.png'
        plt.savefig(path + file_img, bbox_inches='tight')
        plt.close() 
        return '/bigbang/'+file_img
         

    def _get_report_values(self, docids, data=None):
        project_id = data['record']
        wizard_record = request.env['wizard.project.report'].search([])[-1]
        task_obj = request.env['project.task']
        project_obj = self.env['project.project'].browse(project_id)
        stages_selected = []
        
        for elements in wizard_record.stage_select:
            stages_selected.append(elements.id)
        
        if wizard_record.stage_select:
            tasks = task_obj.search([('project_id', '=', project_id), ('stage_id', 'in', stages_selected)], order = 'milestone_id, sequence')
        else:
            tasks = task_obj.search([('project_id', '=', project_id)], order = 'milestone_id, sequence')
            
        vals = []
        file_pie = ''
        file_bar = ''

        if tasks:
            for i in tasks:
                vals.append({
                    'name': i.name,
                    'user': ', '.join([u.name for u in i.user_ids]), 
                    'stage': i.stage_id.name,
                    'milestone': i.milestone_id.name,
                    'progress':i.progress,
                    'description': i.description,
                })

            # build the chart
            file_pie = self.draw_pie_chart(vals, project_id)
            file_bar = self.draw_bar_chart(vals, project_id)
        
        stage =  project_obj.stage_id.name
        if project_obj.last_update_id:
            stage = self.get_selection_label('project.update', 'status', project_obj.last_update_status)
       
        return {
            'tasks': vals,
            'name': project_obj.name,
            'manager': project_obj.user_id.name,
            'partner': project_obj.partner_id.name,
            'date_start': project_obj.date_start,
            'date_end': project_obj.date,
            'description': project_obj.description,
            'stage': stage,
            'last_update': project_obj.last_update_id.name,            
            'last_update_date': project_obj.last_update_id.date,
            'tasks_count': len(project_obj.task_ids),
            'file_pie': file_pie,
            'file_bar': file_bar,
        }
