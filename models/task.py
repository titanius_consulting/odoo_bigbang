
from datetime import datetime
from odoo import fields, models, api, _
from odoo.exceptions import ValidationError

class ProjectTask(models.Model):
    _inherit = 'project.task'

    ## Start Milestone SubModule 
    @api.model
    def _read_group_milestone_ids(self, milestones, domain, order):
        search_domain = [('id', 'in', milestones.ids)]
        if 'default_project_id' in self.env.context:
            search_domain = ['|', ('project_id', '=', self.env.context['default_project_id'])] + search_domain
        milestone_ids = milestones._search(search_domain, order=order)
        return milestones.browse(milestone_ids)
    
    milestone_id = fields.Many2one('project.milestone', string='Hito',
                        tracking=True, index=True,
                        domain="[('project_id', '=', project_id)]",
                        group_expand='_read_group_milestone_ids'
                        )    

    ## Start Gantt SubModule 
    @api.onchange('date_deadline')    
    def _onchange_date_deadline(self):  
        if self.date_deadline and self.milestone_id and self.milestone_id.deadline and self.date_deadline > self.milestone_id.deadline:    
            self.date_deadline =  self.milestone_id.deadline       
            raise ValidationError("La fecha de límite no puede ser mayor que la fecha límite del hito")                       

    @api.onchange('date_from')    
    def _onchange_date_from(self):            
        if self.date_from and self.milestone_id and self.milestone_id.date_start and self.date_from < self.milestone_id.date_start:   
            self.date_from = self.milestone_id.date_start                            
            raise ValidationError("La fecha de inicio no puede ser menor que la fecha de inicio del hito ({self.milestone_id.date_start})")

    @api.onchange('date_to')
    def _onchange_date_to(self):
        if self.date_to and self.date_from and self.date_to < self.date_from:
            self.date_to = self.date_from

    date_from = fields.Date(string='Fecha de inicio', index=True, copy=False)
    date_to = fields.Date(string='Fecha final', index=True, copy=False)
    color = fields.Integer('Color en Gantt', default=4)
    task_priority = fields.Selection([
        ('normal', 'Normal'),
        ('low', 'Low'),        
        ('high', 'High')
    ], string='Prioridad', required=True, default='normal')
    progress = fields.Integer(string='Progreso', tracking=True)


    ## Start Reminder SubModule 
    task_reminder = fields.Boolean("Recordatorio")
    
    @api.model
    def _cron_deadline_reminder(self):
        for task in self.env['project.task'].search(
                [('date_deadline', '!=', None),
                 ('task_reminder', '=', True), ('user_ids', '!=', None)]):
            reminder_date = task.date_deadline
            today = datetime.now().date()

            if task and reminder_date == today:
                template_id = self.env['ir.model.data']._xmlid_lookup(
                    'task_deadline_reminder.email_template_edi_deadline_reminder')[2]
                if template_id:
                    email_template_obj = self.env['mail.template'].browse(
                        template_id)
                    for usr in task.user_ids:
                        data = {'email_to': usr.email}
                        values = email_template_obj.with_context(data).generate_email(task.id,
                           ['subject', 'body_html', 'email_from', 'email_to',
                            'partner_to', 'email_cc', 'reply_to','scheduled_date'])
                        msg_id = self.env['mail.mail'].create(values)
                        if msg_id:
                            msg_id._send()
        return True
    

    ## Start Send Email
    def send_task_by_email(self):
        compose_form = self.env.ref('mail.email_compose_message_wizard_form')
        return {
            'type': 'ir.actions.act_window',
            'name': _("Notificar Entregable por Correo"),
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': {
                'default_res_model': 'mail.compose.message',
                'default_subject': self.name,
                'default_res_ids': self.user_ids.ids,
                'default_body': self.description,                
                'default_attachment_ids': self.attachment_ids.ids
            }
        }
    
    ## Start  Attach Files
    main_attachment_ids = fields.Many2many('ir.attachment', string="Adjuntar documentos principales")
