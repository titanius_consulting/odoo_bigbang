# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.modules.module import get_module_path
import os
import base64

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    
    ### Module Subprojects
    group_allow_subprojects = fields.Boolean("Sub-Proyectos",implied_group='bigbang.group_allow_subprojects')

    ## Module Proposal Project
    proposal_template_file = fields.Binary("Plantilla de propuesta")
    proposal_template_name = fields.Char("Nombre de plantilla")
            
    @api.model
    def get_values(self):
        """get values from the fields"""
        res = super(ResConfigSettings, self).get_values()                
        res.update(
            group_allow_subprojects = self.env['ir.config_parameter'].sudo().get_param('group_allow_subprojects'),
            proposal_template_file = self.env['ir.config_parameter'].sudo().get_param('proposal_template_file'),
            proposal_template_name = self.env['ir.config_parameter'].sudo().get_param('proposal_template_name')
            )
        return res
    
    def set_values(self):
        """Set values in the fields"""
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('group_allow_subprojects', self.group_allow_subprojects)
        if self.proposal_template_file:                                                
            self.write_template_file()
            self.env['ir.config_parameter'].sudo().set_param('proposal_template_file', self.proposal_template_file)        
            self.env['ir.config_parameter'].sudo().set_param('proposal_template_name', self.proposal_template_name)

    def write_template_file(self):
        def isBase64_decodestring(s):
            try:
                return base64.decodestring(s)
            except Exception as e:
                raise ValidationError('Error:', +str(e))
        template_file_path = get_module_path('bigbang') + "/static/docs/" +  self.proposal_template_name        
        object_handle = open(template_file_path, "wb")            
        object_handle.write(isBase64_decodestring(self.proposal_template_file)) 
        object_handle.close()

    def download_proposal_template(self):   
        file_name = self.proposal_template_name if self.proposal_template_name else "template_proposal.docx"                      
        template_file_path = get_module_path('bigbang') + "/static/docs/" +  file_name        
        if not os.path.exists(template_file_path):
            raise ValidationError('El archivo no se encuentra')
        return {
                'type': 'ir.actions.act_url',
                'url': "/bigbang/static/docs/" + file_name,
                'target': 'new',
        }
            