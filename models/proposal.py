from datetime import datetime
from odoo import fields, models, api, _
from odoo.modules.module import get_module_path
from docxtpl import DocxTemplate
import os
import io
import json
from odoo.tools import date_utils
from odoo.http import request

class Proposal(models.Model):
    _name = 'project.proposal'
    _inherit = ['mail.thread','mail.activity.mixin']
    _description = "Propuesta de proyecto"
    _order = 'create_date desc, id desc'

    @api.model
    def create(self, vals):
        if not vals.get('name') or vals['name'] == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('project.proposal') or _('New')    
        proposal = super(Proposal, self).create(vals)              
        return proposal
    
    @api.onchange('kanban_state')
    def _onchange_kanban_state(self):     
        if self.kanban_state in ['done', 'blocked']:
            self.state = 'done'
        elif self.kanban_state == 'normal':
            self.state = 'draft'
        self.kanban_state_date = datetime.now().strftime('%Y-%m-%d')
    
    def send_by_email(self):
        if self.state == 'draft':
            self.state = 'send'
        compose_form = self.env.ref('mail.email_compose_message_wizard_form')
        template = self.env.ref('bigbang.email_template_proposal_partner', raise_if_not_found=False)
        return {
            'type': 'ir.actions.act_window',
            'name': _("Notificar Propuesta por Correo"),
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': {
                'default_res_model': 'mail.compose.message',
                'default_subject': self.title,
                'default_res_ids': self.ids,
                'default_partner_ids': self.partner_id.ids,                
                'default_use_template': bool(template),
                'default_template_id': template and template.id,          
                'default_attachment_ids': self.main_attachment_ids.ids
            }
        }
    
    
    def get_docx_report(self, data, response):     
        template_file_name = self.env['ir.config_parameter'].sudo().get_param('proposal_template_name')
        if not template_file_name:
            template_file_name = "template_proposal.docx" 
        template_file_path = get_module_path('bigbang') + "/static/docs/" +   template_file_name
        proposal_obj = request.env['project.proposal'].search([('id', '=', data['record'])])
        terms_type = self.get_selection_label('project.proposal', 'terms_type', proposal_obj.terms_type)
        variables = {
            "create_date": str(proposal_obj.create_date.strftime("%m/%d/%Y")),
            "name": proposal_obj.name,
            "title": proposal_obj.title,
            "partner_id": proposal_obj.partner_id.name,
            "terms": str(proposal_obj.terms_value) + ' ' + str(terms_type),
            "total_amount": str(proposal_obj.currency_id.symbol) + '. ' + "{:.2f}".format(proposal_obj.total_amount)
        }
        template_document = DocxTemplate(template_file_path)
        template_document.render(variables)               
        output = io.BytesIO()
        template_document.save(output)        
        output.seek(0)
        response.stream.write(output.read())
        output.close()    
    
    def action_download_word(self):                
        data = {
            'ids': self.ids,
            'model': self._name,
            'record': self.id,
        }
        return {
            'type': 'ir.actions.report',
            'data': {'model': 'project.proposal',
                     'options': json.dumps(data, default=date_utils.json_default),
                     'output_format': 'docx',
                     'report_name': self.name,
                     },
            'report_type': 'docx'
        }
    
    def build_project(self):
        self.kanban_state = 'done'
        self.state = 'done'
        self.project_id = self.env['project.project'].create(
            {'partner_id': self.partner_id.id,
             'name': self.title,             
             'tag_ids':self.tag_ids,
             'proposal_id': self.id})   
        return {
            'name': _('Project'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'project.project',
            'type': 'ir.actions.act_window',
            'context': {'active_id': self.project_id.id},
            'res_id': self.project_id.id,     
            'flags':{'mode':'edit'}    
        }     
    
    def view_project(self):
        if self.project_id:
            return {
                'name': _('Project'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'project.project',
                'type': 'ir.actions.act_window',
                'context': {'active_id': self.project_id.id},
                'res_id': self.project_id.id
            }  

    name = fields.Char(string="Número", copy=False, default=lambda self: _('New'))
    title = fields.Char(string="Título", help="Título referencial", required=True)
    create_date = fields.Date(string="Fecha del Documento", default=lambda self: datetime.now().strftime('%Y-%m-%d'), required=True)
    user_id = fields.Many2one('res.users', string='Remitente', default=lambda self: self.env.user, track_visibility='onchange')
    terms_type = fields.Selection([
            ('days', 'Días'),
            ('weeks', 'Semanas'),
            ('months', 'Meses')], 
            string='Plazo', copy=False, default='weeks')
    terms_value = fields.Integer(string='Valor de plazo')
    state = fields.Selection([
        ('draft', 'Registrado'),
        ('send', 'Presentado'),
        ('done', 'Terminado')], 
        string='Estado', copy=False, index=True, track_visibility='onchange', default='draft')
    partner_id = fields.Many2one('res.partner', string='Cliente', readonly=True, required=True,
                                 states={'draft': [('readonly', False)],
                                         'send': [('readonly', False)]},
                                 change_default=True, index=True, track_visibility='always')
    total_amount = fields.Float(string='Costo Total')
    tag_ids = fields.Many2many('project.tags', string='Etiquetas')
    description = fields.Html(string='Descripción')
    priority = fields.Selection([
        ('0', 'Normal'),
        ('1', 'Importante')], 
        default='0', index=True, string="Starred", tracking=True)
    currency_id = fields.Many2one("res.currency", string="Moneda", default=lambda self: self.env.user.company_id.currency_id)
    main_attachment_ids = fields.Many2many('ir.attachment', string="Adjuntar documento de propuesta")
    kanban_state = fields.Selection([
        ('normal', 'En Proceso'),
        ('done', 'Aprobado'),
        ('blocked', 'Rechazado')], 
        string='Resultado', copy=False, default='normal', required=True, help="Establecer resultado final", track_visibility='onchange')
    kanban_state_date = fields.Date(string="Fecha de Resultado")
    project_id = fields.Many2one('project.project', string='Proyecto')
    
    
    #### Util #####
    def get_selection_label(self, object, field_name, field_value):
        return _(dict(self.env[object].fields_get(allfields=[field_name])[field_name]['selection'])[field_value])