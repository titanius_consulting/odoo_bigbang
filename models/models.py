from odoo import api, fields, models

class View(models.Model):
    _inherit = 'ir.ui.view'
    
    type = fields.Selection(selection_add=[
        ('ganttview', "Gantt View")
        ], ondelete={'ganttview': 'cascade'})


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.model
    def default_get(self, fields):
        rec = super(MailComposer, self).default_get(fields)
        ctx = self.env.context
        if ctx.get('default_subject'):
            rec.update({'subject': ctx['default_subject']})
        return rec
