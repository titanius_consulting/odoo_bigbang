

from odoo import fields, models, api, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError

class Project(models.Model):
    _inherit = 'project.project'

    @api.model
    def create(self, vals):         
        vals['type_ids'] = self.env['project.task.type'].search([('case_default', '=', True)])        
        return super(models.Model, self).create(vals)
    
    label_tasks = fields.Char(string='Use Tasks as', default='Entregables', help="Label used for the tasks of the project.", translate=True)
        
    ## Start Subproject SubModule 
    # If depth == 1, return only direct children
    # If depth == 3, return children to third generation
    # If depth <= 0, return all children without depth limit
    def _get_all_subproject(self, depth=0):
        children = self.mapped('child_ids')
        if not children:
            return self.env['project.project']
        if depth == 1:
            return children
        return children + children._get_all_subproject(depth - 1)
    
    @api.depends('child_ids')
    def _compute_subproject_count(self):
        for project in self:
            project.subproject_count = len(project.child_ids)
                
    def open_subprojects(self):
        return {
                'type': 'ir.actions.act_window',
                'name': _("Sub-Proyectos"),
                'res_model': 'project.project',
                'view_mode': 'kanban,tree,form',
                'target':'self',
                'domain': [('id','in',self.child_ids.ids)]
            }

    def unlink(self):
        for project in self:
            if project.child_ids:
                raise ValidationError("No puede eliminar proyectos que tienen subproyectos")
            if project.tasks:
                raise UserError("No puede eliminar un subproyecto que contenga entregables.")
        return super(Project,self).unlink()

    parent_id = fields.Many2one('project.project', string='Proyecto Padre', index=True)
    child_ids = fields.One2many('project.project', 'parent_id', string="Sub-Proyectos")
    subproject_count = fields.Integer("Contador Sub-Proyectos", compute='_compute_subproject_count')
    allow_subprojects = fields.Boolean('Sub-Proyectos',default=lambda self: self.env.user.has_group('bigbang.group_allow_subprojects'))

    ## Start Account SubModule 
    account_orders  = fields.One2many('account.move', 'project_id', string="Facturas")        
    def write(self, vals):        
        if 'account_orders' in vals:
            for item in vals['account_orders']:
                if item[2]:
                    item[2]['move_type'] = 'out_invoice'
        return super(Project, self).write(vals)

    ## Start Proposal
    proposal_id = fields.Many2one('project.proposal', string='Propuesta')

    def print_project_report_pdf(self):
        active_record = self.id
        data = {
            'ids': self.ids,
            'model': self._name,
            'record': active_record,
        }
        return self.env.ref('bigbang.report_project_pdf').report_action(self, data=data)


class ProjectMilestone(models.Model):
    _inherit = 'project.milestone'

    date_start = fields.Date(string='Fecha Inicio')

    _sql_constraints = [
        ('project_milestone_date_greater',
         'check(deadline >= date_start)', 
         'Error! Fecha Inicio debe ser menor que Fecha Límite.')
    ]


class ProjectTaskType(models.Model):
    _inherit = "project.task.type"

    case_default = fields.Boolean(
        string="Predeterminado",
        help="Si marca este campo, esta etapa se propondrá por defecto en cada nuevo proyecto. " 
        + "No asignará esta etapa a proyectos existentes..",
    )

