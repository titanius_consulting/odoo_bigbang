# -*- coding: utf-8 -*-
{
    'name': "bigbang",
    'summary': """    Adaptación del módulo de proyecto para la empresa Bing Bang Group  """,
    'description': """
          
    """,
    'author': "Titanius Consulting",
    'website': "http://www.titanius.com",
    'category': 'Services/Project',
    'version': '0.1',
    'depends': ['base', 'web', 'mail', 'project','account'],
    'data': [
        'security/ir.model.access.csv',
        'security/project_security.xml',
        'wizard/project_report_wizard_view.xml',
        'report/project_report_view.xml',
        'views/project_view.xml',
        'views/task_view.xml',
        'views/proposal_view.xml',
        'views/res_config_settings_views.xml',
        'views/deadline_reminder_cron.xml',
        'data/deadline_reminder_data.xml',
        'data/proposal_email_data.xml',
        'data/product_data.xml',
    ],
    'assets': {
        "web.assets_backend": [
            "/bigbang/static/lib/dhtmlxGantt/sources/dhtmlxgantt.js",
            "/bigbang/static/lib/dhtmlxGantt/sources/api.js",
            "/bigbang/static/lib/dhtmlxGantt/sources/dhtmlxgantt.css",            
            "/bigbang/static/src/css/gantt.css",
            "/bigbang/static/src/js/gantt_renderer.js",
            "/bigbang/static/src/js/gantt_view.js",
            "/bigbang/static/src/js/gantt_model.js",
            "/bigbang/static/src/js/gantt_controller.js",
            '/bigbang/static/src/js/action_manager.js',
        ],
        "web.assets_qweb": [
            'bigbang/static/src/xml/*.xml',
        ]
    },
    'external_dependencies':{
        'python' : ['matplotlib','docxtpl'],
    },
    'installable': True,
    'application': False,
    'auto_install': False,
}
